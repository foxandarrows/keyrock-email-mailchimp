var ObjectID = require("mongodb").ObjectID;

module.exports = function(app, db) {
  app.get("/email/:id", (req, res) => {
    const id = req.params.id;
    const details = { _id: new ObjectID(id) };
    db.collection("email").findOne(details, (err, item) => {
      if (err) {
        res.send({ error: "An error has occured" });
      } else {
        res.send(item);
      }
    });
  });

  app.post("/email", (req, res) => {
    const email = {
      first_name: req.body.first_name,
      last_name: req.body.last_name,
      email: req.body.email
    };
    console.log(db);

    db.collection("email").insert(email, (err, result) => {
      if (err) {
        res.send({ error: "An error has occurred" });
      } else {
        res.send(result.ops[0]);
      }
    });
  });
};
